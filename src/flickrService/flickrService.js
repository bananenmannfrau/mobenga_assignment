var flickrService =
(function(){
  var flickrBaseUrl = "https://api.flickr.com/services/rest/?";
  var api_key = "api_key=b54580f369a7eeebecb2004dc429d08f&";
  //creating functions here to keep future implementations open
  //values could be passed in
  var numberOfPhotos = function(number){return "&per_page=" + number;};
  var format = function(format){ return "&format=" + format;};

  function searchPhotos(searchParam, number){
    var script = document.createElement('script');
    var requestParams = api_key + "method=flickr.photos.search&text=" + searchParam + "&extras=url_m,description" + numberOfPhotos(number) + format("json");
    script.src = flickrBaseUrl + requestParams;
    document.getElementsByTagName('head')[0].appendChild(script);
  }

  return {
    searchPhotos: searchPhotos
  };
}
)();
