var mainModule = (function(){
    //VARIABLES
    var searchBar = document.querySelector('.searchBar');
    var searchButton = document.querySelector('.searchButton');
    var showGallery = document.querySelector('.showGallery');
    var previousPhoto = document.querySelector('.previousPhoto');
    var nextPhoto = document.querySelector('.nextPhoto');
    var galleryContainer = document.querySelector('.gallery');
    var photoChooserContainer = document.querySelector('.photoChooser');
    var closeGallery =  document.querySelector('.closeGallery');
    var galleryPhotoContainer = document.querySelector('.galleryPhotoContainer');
    var containers = [photoChooserContainer, galleryContainer, showGallery, searchButton, searchBar];

    //EVENT LISTENERS
    if(previousPhoto){
      previousPhoto.addEventListener("click", function(e){
        if(gallery.isInitiated()){
          gallery.showPrevious();
        }
        e.preventDefault();
        e.stopPropagation();
      });
    }

    if(nextPhoto){
      nextPhoto.addEventListener("click", function(e){
        if(gallery.isInitiated()){
          gallery.showNext();
        }
        e.preventDefault();
        e.stopPropagation();
      });
    }


    if(showGallery){
      showGallery.addEventListener("click", function(e){
        if(photoStore.getPhotos().length > 0){
          gallery.initiateGallery(photoChooser.getSelectedPhotos(), galleryPhotoContainer);
          toggleVisibilties(containers);
        }
        e.preventDefault();
        e.stopPropagation();
      });
    }

    if(closeGallery){
      closeGallery.addEventListener("click", function(e){
        gallery.closeGallery();
        toggleVisibilties(containers);
        e.preventDefault();
        e.stopPropagation();
      });
    }

    if(searchButton){
      searchButton.addEventListener("click", function(e){
        if(searchBar.value !== ""){
            flickrService.searchPhotos(searchBar.value, 10);
            photoChooser.clearSelectedPhotos();
          }
        e.preventDefault();
        e.stopPropagation();
       });
    }


    //FUNCTIONS
    function addPhotosToPhotoChooserContainer(photos){
      photoChooser.clearPhotosFromDOM(photoChooserContainer);
      photoChooser.addPhotosToDOM(photoChooserContainer, photos);
    }

    function toggleVisibilties(containers){
      containers.forEach(function (container){
        helperMethods.toggleClass(container, "hidden");
      });
    }
    // returning this here, so that the JSONP function has access
    // to the method
    return {
      addPhotosToPhotoChooserContainer: addPhotosToPhotoChooserContainer
    };
})();

//JSONP function for flickr api
function jsonFlickrApi(rsp) {
  photoStore.addPhotos(rsp.photos.photo);
  mainModule.addPhotosToPhotoChooserContainer(photoStore.getPhotos());
}
