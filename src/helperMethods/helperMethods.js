var helperMethods =
(function(){

  function toggleClass(element, klass){
    element.classList.toggle(klass);
  }

  return{
    toggleClass: toggleClass
  };
})();
