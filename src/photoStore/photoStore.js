var photoStore =
(function(){
  var availablePhotos = [];

  function addPhotos(photos){
    availablePhotos = photos.map(function(photo){
      return {
            url_m: photo.url_m,
            description: photo.description,
            id: photo.id
      };
    });
  }

  function getPhotos() {
    return availablePhotos;
  }

  return {
    addPhotos: addPhotos,
    getPhotos: getPhotos
  };
})();
