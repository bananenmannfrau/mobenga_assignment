var gallery =
(function(){
  var counter = 0;
  var container = {};
  var initiated = false;
  var photos = [];


  function getPhoto(index){
    return photos[index];
  }

  function amountOfPhotos(){
    return photos.length;
  }

  function setContainer(galleryContainer){
    container = galleryContainer;
  }
  //start adding the first photo from the selectedPhotos to the container
  function initiateGallery(galleryPhotos, galleryContainer){
    counter = 0;
    setContainer(galleryContainer);
    photos = galleryPhotos;
    initiated = true;
    var photo = photos[0];
    addPhotoToGallery(photo);
  }

  function addPhotoToGallery(photo){
    var img = document.createElement("img");
    img.src = photo.url_m;
    img.alt = photo.description._content;
    container.appendChild(img);
  }

  function closeGallery(){
    initiated = false;
    clearContainer();
  }

  function clearContainer(){
    container.innerHTML = "";
  }

  function showNext(){
    clearContainer();
    addPhotoToGallery(nextPhoto());
  }

  function nextPhoto(){
    increaseCounter();
    return getPhoto(counter);
  }

  function showPrevious(){
    clearContainer();
    addPhotoToGallery(previousPhoto());
  }

  function previousPhoto() {
    decreaseCounter();
    return getPhoto(counter);
  }

  function decreaseCounter(){
    if(counter === 0){
      counter = amountOfPhotos() - 1;
    }
    else{
      --counter;
    }
  }

  function increaseCounter(){
    counter = (counter + 1) % amountOfPhotos();
  }

  function isInitiated(){
    return initiated;
  }

  return {
    initiateGallery: initiateGallery,
    showNext: showNext,
    showPrevious: showPrevious,
    isInitiated: isInitiated,
    closeGallery: closeGallery
  };
}
)();
