var photoChooser =
(function(){
  var selectedPhotos = [];

  function addPhotosToDOM(container, photos){
    //binding the Photos to this document fragment first before applying it to the document
    //to save expensive reflows
    var documentFragment = document.createDocumentFragment();

    //this creates the dom element for each foto
    photos.forEach(function(photo) {
      if(photo.url_m !== undefined){
        var img = document.createElement("img");
        img.src = photo.url_m;
        img.alt = photo.description._content;
        //adding the eventListener here.
        //The addOrRemoveFromGallery is still not availaible as a public method
        img.onclick = addOrRemoveFromGallery;
        documentFragment.appendChild(img);
      }
    });
    container.appendChild(documentFragment);
  }

  function clearPhotosFromDOM(container){
    container.innerHTML = "";
  }

  function addOrRemoveFromGallery(photo){
    var image = getMouseEventImage(photo);
    var url = image.currentSrc;
    helperMethods.toggleClass(image, "selected");

    var selectedPhoto = getSelectedPhoto(photoStore.getPhotos(), url);
    var index = selectedPhotos.indexOf(selectedPhoto);

    if (index > -1) {
      removeFromGallery(index);
    }
    else{
      addToGallery(selectedPhoto);
    }
  }
  function addToGallery(photo){
    selectedPhotos.push(photo);
  }

  function removeFromGallery(index){
    selectedPhotos.splice(index, 1);
  }

  function getMouseEventImage(event){
      return event.srcElement;
  }

  function getSelectedPhoto(array, url){
    //this gets the photos and filters them by the url to get the selected photo
    //Array.filter() returns an array. So [0] is needed in the end
    return array.filter(function (photo){
        return photo.url_m === url;
      })[0];
  }

  function getSelectedPhotos(){
    return selectedPhotos;
  }

  function clearSelectedPhotos(){
    selectedPhotos = [];
  }

  return {
    addPhotosToDOM: addPhotosToDOM,
    clearPhotosFromDOM: clearPhotosFromDOM,
    getSelectedPhotos: getSelectedPhotos,
    clearSelectedPhotos: clearSelectedPhotos
  };
})();
