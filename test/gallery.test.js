describe("gallery", function() {
  var photos, container;
  beforeEach(function() {
    container = document.createElement('div');
    photos = [
      {
        url_m: 'test1',
        description : {
          _content: "test one"
        },
        id: 1
      },
      {
        url_m: 'test2',
        description : {
          _content: "test two"
        },
        id: 2
      },
      {
        url_m: 'test3',
        description : {
          _content: "test three"
        },
        id: 3
      }
    ];
  });

  afterEach(function() {
    container.innerHTML = '';
  });

  it("initiates", function() {
    gallery.initiateGallery(photos, container);
    expect(container.innerHTML).toEqual('<img src="test1" alt="test one">');
    expect(gallery.isInitiated()).toEqual(true);
  });

  it("shows the next photo", function() {
    gallery.initiateGallery(photos, container);
    gallery.showNext();
    expect(container.innerHTML).toEqual('<img src="test2" alt="test two">');
  });

  it("shows the previous photo", function() {
    gallery.initiateGallery(photos, container);
    gallery.showPrevious();
    expect(container.innerHTML).toEqual('<img src="test3" alt="test three">');
  });

  it("closes the gallery", function(){
    gallery.closeGallery();
    expect(container.innerHTML).toEqual('');
    expect(gallery.isInitiated()).toEqual(false);
  });


});
