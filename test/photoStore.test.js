describe("photoStore", function() {

  var photos;


  beforeEach(function() {
    photos = [
      {
        url_m: 'test1',
        description : {
          _content: "test one"
        },
        id: 1
      }
    ];
  });

  it("adds and gets photos", function() {
    photoStore.addPhotos(photos);
    expect(photoStore.getPhotos()).toEqual([
      {
        url_m: 'test1',
        description : {
          _content: "test one"
        },
        id: 1
      }
    ]);
  });
});
