describe("photoChooser", function() {

  var container, photos;


  beforeEach(function() {
    container = document.createElement('div');
    photos = [
      {
        url_m: 'test1',
        description : {
          _content: "test one"
        },
        id: 1
      }
    ];
  });

  it("adds to gallery", function() {
    photoChooser.addPhotosToDOM(container, photos);
    expect(container.innerHTML).toEqual('<img src="test1" alt="test one">');
  });

  it("removes from gallery", function() {
    photoChooser.addPhotosToDOM(container, photos);
    expect(container.innerHTML).toEqual('<img src="test1" alt="test one">');

    photoChooser.clearPhotosFromDOM(container);
    expect(container.innerHTML).toEqual('');
  });
});
