To build the project first run
```
npm install
```
to install the dependencies.

After the task is complete run
```
gulp build
```
to build the Javacript and Css file.

Now open `index.html` in Chrome.


Tests
==
Some example Tests have been added.

run

```
gulp test
```
to execute them.

It is important to know that only public methods, declared in the return of the modules can be tested.




Bugs
==

Opening the `index.html` in Firefox and searching for photos, results in them being too stretched or not shown at all. CSS cross-browser error.

Not important when only target is Chrome-Desktop though.
