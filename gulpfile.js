var gulp = require('gulp');
var Server = require('karma').Server;
var concat = require('gulp-concat');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;
var stylus = require('gulp-stylus');




gulp.task('default', function() {
  // place code for your default task here
});


gulp.task('build',['js', 'stylus']);


gulp.task('test', function (done) {
  new Server({
    configFile: __dirname + '/karma.conf.js',
    singleRun: true
  }, function() {
        done();
    }).start();
});

gulp.task('watch-test', function (done) {
  new Server({
    configFile: __dirname + '/karma.conf.js',
  }, function() {
        done();
    }).start();
});

gulp.task('js', function() {
  return gulp.src('./src/**/*.js')
    .pipe(concat('script.js'))
    .pipe(gulp.dest('./dist/'));
});

gulp.task('stylus', function () {
  return gulp.src('./src/**/*.styl')
    .pipe(concat('styles.js'))
    .pipe(stylus())
    .pipe(gulp.dest('./dist/'));
});

gulp.watch(["index.html"], reload);
gulp.watch('src/**/*.styl', ['stylus']);
gulp.watch('dist/styles.css', reload);
gulp.watch("src/**/*.js", ['js']);
gulp.watch('dist/script.js', reload);


gulp.task('serve', ['js'], function () {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });


});
